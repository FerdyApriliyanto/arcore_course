import 'package:arcore_course/screens/avengers_object_screen.dart';
import 'package:arcore_course/screens/custom_object_screen.dart';
import 'package:arcore_course/screens/earth_object_screen.dart';
import 'package:arcore_course/screens/geometric_object_screen.dart';
import 'package:arcore_course/screens/home_screen.dart';
import 'package:arcore_course/screens/quotes_object_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter AR Course',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => const HomeScreen(),
        '/custom-object-screen': (context) => const CustomObjectScreen(),
        '/geometric-object-screen': (context) => GeometricObjectScreen(),
        '/earth-object-screen': (context) => EarthObjectScreen(),
        '/avengers-object-screen': (context) => AvengersObjectScreen(),
        '/quotes-object-screen': (context) => QuotesObjectScreen(),
      },
    );
  }
}
