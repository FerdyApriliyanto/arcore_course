import 'package:ar_flutter_plugin_flutterflow/datatypes/config_planedetection.dart';
import 'package:ar_flutter_plugin_flutterflow/widgets/ar_view.dart';
import 'package:arcore_course/utils/custom_object_utils.dart';
import 'package:flutter/material.dart';

class CustomObjectScreen extends StatefulWidget {
  const CustomObjectScreen({super.key});

  @override
  State<CustomObjectScreen> createState() => _CustomObjectScreenState();
}

class _CustomObjectScreenState extends State<CustomObjectScreen> {
  final utils = CustomObjectUtils();

  @override
  void dispose() {
    super.dispose();

    utils.sessionManager!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Custom 3D Object'),
        centerTitle: true,
      ),
      body: SizedBox(
        child: Stack(
          children: [
            ARView(
              planeDetectionConfig: PlaneDetectionConfig.horizontalAndVertical,
              onARViewCreated: utils.whenARViewCreated,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 8, bottom: 8),
              child: Align(
                alignment: Alignment.bottomRight,
                child: ElevatedButton(
                    onPressed: () => utils.removeEveryAnchor(),
                    child: const Text('Remove Object')),
              ),
            )
          ],
        ),
      ),
    );
  }
}
