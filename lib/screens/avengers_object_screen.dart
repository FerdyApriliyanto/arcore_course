import 'package:arcore_course/utils/avengers_object_utils.dart';
import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
import 'package:flutter/material.dart';

class AvengersObjectScreen extends StatelessWidget {
  AvengersObjectScreen({super.key});

  final utils = AvengersObjectUtils();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Avengers Object from Assets'),
        centerTitle: true,
      ),
      body: ArCoreView(
          enableTapRecognizer: true,
          onArCoreViewCreated: utils.onArCoreViewCreated),
    );
  }
}
