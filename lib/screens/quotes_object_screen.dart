import 'package:arcore_course/utils/quotes_object_utils.dart';
import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
import 'package:flutter/material.dart';

class QuotesObjectScreen extends StatelessWidget {
  QuotesObjectScreen({super.key});

  final utils = QuotesObjectUtils();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Quotes Object from URL'),
        centerTitle: true,
      ),
      body: ArCoreView(
          enableTapRecognizer: true,
          onArCoreViewCreated: utils.onArCoreViewCreated),
    );
  }
}
