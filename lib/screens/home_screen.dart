import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('AR Demo'),
        centerTitle: true,
      ),
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () =>
                    Navigator.pushNamed(context, '/custom-object-screen'),
                child: const Text('Custom 3D Object')),
            const SizedBox(
              height: 16,
            ),
            ElevatedButton(
                onPressed: () =>
                    Navigator.pushNamed(context, '/geometric-object-screen'),
                child: const Text('Geometric 3D Object')),
            const SizedBox(
              height: 16,
            ),
            ElevatedButton(
                onPressed: () =>
                    Navigator.pushNamed(context, '/earth-object-screen'),
                child: const Text('Earth 3D Object')),
            const SizedBox(
              height: 16,
            ),
            ElevatedButton(
                onPressed: () =>
                    Navigator.pushNamed(context, '/avengers-object-screen'),
                child: const Text('Avengers Image Object')),
            const SizedBox(
              height: 16,
            ),
            ElevatedButton(
                onPressed: () =>
                    Navigator.pushNamed(context, '/quotes-object-screen'),
                child: const Text('Quotes Image Object')),
          ],
        ),
      ),
    );
  }
}
