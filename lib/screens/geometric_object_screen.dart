import 'package:arcore_course/utils/geometric_object_utils.dart';
import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
import 'package:flutter/material.dart';

class GeometricObjectScreen extends StatelessWidget {
  GeometricObjectScreen({super.key});

  final utils = GeometricObjectUtils();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Geometric 3D Object'),
        centerTitle: true,
      ),
      body: ArCoreView(onArCoreViewCreated: utils.onArCoreViewCreated),
    );
  }
}
