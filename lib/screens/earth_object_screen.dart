import 'package:arcore_course/utils/earth_object_utils.dart';
import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
import 'package:flutter/material.dart';

class EarthObjectScreen extends StatelessWidget {
  EarthObjectScreen({super.key});

  final utils = EarthObjectUtils();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Earth 3D Object'),
        centerTitle: true,
      ),
      body: ArCoreView(onArCoreViewCreated: utils.onArCoreViewCreated),
    );
  }
}
