import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
import 'package:flutter/services.dart';
import 'package:vector_math/vector_math_64.dart' as vector64;

class AvengersObjectUtils {
  ArCoreController? coreController;

  void onArCoreViewCreated(ArCoreController controller) {
    coreController = controller;

    coreController!.onPlaneTap = onPlaneTap;
  }

  void onPlaneTap(List<ArCoreHitTestResult> hitResult) {
    final tapPosition = hitResult.first;

    // DISPLAY AVENGERS OBJECT FROM ASSETS
    displayAvengersObject(tapPosition);
  }

  Future<void> displayAvengersObject(ArCoreHitTestResult tapPosition) async {
    final bytesImage =
        (await rootBundle.load('images/iron_man.png')).buffer.asUint8List();

    final imagePositionRotation = ArCoreNode(
        image: ArCoreImage(bytes: bytesImage, width: 500, height: 600),
        position: tapPosition.pose.translation + vector64.Vector3(0, 0, 0),
        rotation: tapPosition.pose.rotation + vector64.Vector4(0, 0, 0, 0));

    coreController?.addArCoreNodeWithAnchor(imagePositionRotation);
  }
}
