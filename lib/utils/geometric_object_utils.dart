import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
import 'package:flutter/material.dart';
import 'package:vector_math/vector_math_64.dart' as vector64;

class GeometricObjectUtils {
  ArCoreController? coreController;

  void onArCoreViewCreated(ArCoreController controller) {
    coreController = controller;

    displayCube(coreController!);
    displayCylinder(coreController!);
  }

  void displayCube(ArCoreController controller) {
    final materials = ArCoreMaterial(color: Colors.red, metallic: 100);

    final arCube = ArCoreCube(
        size: vector64.Vector3(0.5, 0.5, 0.5), materials: [materials]);

    final arNode =
        ArCoreNode(shape: arCube, position: vector64.Vector3(-0.5, 0.5, -3.5));

    // DISPLAY 3D OBJECT
    coreController?.addArCoreNode(arNode);
  }

  void displayCylinder(ArCoreController controller) {
    final materials = ArCoreMaterial(color: Colors.blue, metallic: 2);

    final arCylinder =
        ArCoreCylinder(radius: 0.5, height: 0.5, materials: [materials]);

    final arNode = ArCoreNode(
        shape: arCylinder, position: vector64.Vector3(0.0, -0.5, -2.0));

    // DISPLAY 3D OBJECT
    coreController?.addArCoreNode(arNode);
  }
}
