import 'package:ar_flutter_plugin_flutterflow/datatypes/hittest_result_types.dart';
import 'package:ar_flutter_plugin_flutterflow/datatypes/node_types.dart';
import 'package:ar_flutter_plugin_flutterflow/managers/ar_anchor_manager.dart';
import 'package:ar_flutter_plugin_flutterflow/managers/ar_location_manager.dart';
import 'package:ar_flutter_plugin_flutterflow/managers/ar_object_manager.dart';
import 'package:ar_flutter_plugin_flutterflow/managers/ar_session_manager.dart';
import 'package:ar_flutter_plugin_flutterflow/models/ar_anchor.dart';
import 'package:ar_flutter_plugin_flutterflow/models/ar_hittest_result.dart';
import 'package:ar_flutter_plugin_flutterflow/models/ar_node.dart';
import 'package:vector_math/vector_math_64.dart' as vector64;

class CustomObjectUtils {
  ARSessionManager? sessionManager;
  ARObjectManager? objectManager;
  ARAnchorManager? anchorManager;

  List<ARNode> allNode = [];
  List<ARAnchor> allAnchor = [];

  void whenARViewCreated(
      ARSessionManager arSessionManager,
      ARObjectManager arObjectManager,
      ARAnchorManager arAnchorManager,
      ARLocationManager arLocationManager) {
    sessionManager = arSessionManager;
    objectManager = arObjectManager;
    anchorManager = arAnchorManager;

    sessionManager!.onInitialize(
        showFeaturePoints: false,
        customPlaneTexturePath: 'assets/triangle.png',
        showPlanes: true,
        showWorldOrigin: true,
        handlePans: true,
        handleRotation: true);

    objectManager!.onInitialize();

    sessionManager!.onPlaneOrPointTap = whenPlaneDetectedAndUserTapped;
  }

  Future<void> whenPlaneDetectedAndUserTapped(
      List<ARHitTestResult> tapResults) async {
    final userHitTestResult = tapResults
        .firstWhere((userTap) => userTap.type == ARHitTestResultType.plane);

    //NEW ANCHOR
    final newPlaneARAnchor =
        ARPlaneAnchor(transformation: userHitTestResult.worldTransform);

    bool? isAnchorAdded = await anchorManager!.addAnchor(newPlaneARAnchor);

    if (isAnchorAdded!) {
      allAnchor.add(newPlaneARAnchor);

      //NEW NODE / 3D OBJECT
      final newNode3dObject = ARNode(
          type: NodeType.webGLB,
          uri:
              'https://firebasestorage.googleapis.com/v0/b/ar-3d-objects.appspot.com/o/puss_in_boots_pocket_shrek.glb?alt=media&token=cef8fe0b-a842-4c1d-9ad3-095bafb19e7f',
          scale: vector64.Vector3(0.2, 0.2, 0.2),
          position: vector64.Vector3(0, 0, 0),
          rotation: vector64.Vector4(1.0, 0, 0, 0));

      bool? isNewNodeAddedToNewAnchor = await objectManager!
          .addNode(newNode3dObject, planeAnchor: newPlaneARAnchor);

      if (isNewNodeAddedToNewAnchor!) {
        allNode.add(newNode3dObject);
      } else {
        sessionManager!.onError!('Attaching Node to Anchor Failed');
      }
    } else {
      sessionManager!.onError!('Adding Anchor Failed');
    }
  }

  Future<void> removeEveryAnchor() async {
    for (var eachAnchor in allAnchor) {
      anchorManager!.removeAnchor(eachAnchor);
    }

    allAnchor = [];
  }
}
