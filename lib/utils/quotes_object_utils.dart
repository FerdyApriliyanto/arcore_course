import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
import 'package:flutter/services.dart';
import 'package:vector_math/vector_math_64.dart' as vector64;

class QuotesObjectUtils {
  ArCoreController? coreController;

  void onArCoreViewCreated(ArCoreController controller) {
    coreController = controller;

    coreController!.onPlaneTap = onPlaneTap;
  }

  void onPlaneTap(List<ArCoreHitTestResult> hitResult) {
    final tapPosition = hitResult.first;

    // DISPLAY QUOTES OBJECT FROM URL
    displayQuotesObject(tapPosition);
  }

  Future<void> displayQuotesObject(ArCoreHitTestResult tapPosition) async {
    final ByteData byteDataNetwork = await NetworkAssetBundle(Uri.parse(
            'https://img.freepik.com/free-vector/inspirational-quote-watercolour-background_1048-18831.jpg?size=338&ext=jpg&ga=GA1.1.1700460183.1712275200&semt=sph'))
        .load('');

    // CONVERT TO Uint8List
    final bytesImage = byteDataNetwork.buffer.asUint8List();

    final imagePositionRotation = ArCoreNode(
        image: ArCoreImage(bytes: bytesImage, width: 500, height: 500),
        position: tapPosition.pose.translation + vector64.Vector3(0, 0, 0),
        rotation: tapPosition.pose.rotation + vector64.Vector4(0, 0, 0, 0));

    coreController?.addArCoreNodeWithAnchor(imagePositionRotation);
  }
}
