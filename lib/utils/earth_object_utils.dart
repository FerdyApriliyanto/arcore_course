import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vector_math/vector_math_64.dart' as vector64;

class EarthObjectUtils {
  ArCoreController? coreController;

  void onArCoreViewCreated(ArCoreController controller) {
    coreController = controller;

    displayEarth(coreController!);
  }

  Future<void> displayEarth(ArCoreController controller) async {
    final ByteData earthTextureBytes =
        await rootBundle.load('assets/earth_map.jpg');

    final materials = ArCoreMaterial(
        color: Colors.red,
        textureBytes: earthTextureBytes.buffer.asUint8List());

    final arSphere = ArCoreSphere(materials: [materials]);

    final arNode =
        ArCoreNode(shape: arSphere, position: vector64.Vector3(0, 0, -1.5));

    // DISPLAY 3D OBJECT
    coreController?.addArCoreNode(arNode);
  }
}
